import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Programm, mis võtab tekstifailina etteantud üldkogumist
 * soovitud suurusega valimi.
 * Andmete asukoha ja valimi suuruse sisestab kasutaja
 * vastavas kasutajaliideses. Sisestamise järel peab vajutama
 * klahvi "Enter".
 * Tulemus väljastatakse eraldi aknas.
 * 
 * Rakendab klasse JuhuslikValim ning ValimimahuExeption.
 * 
 * @author Kai Sarv
 * @since 20.11.2015
 *
 */
public class Otsustaja {

	static String[] yldkogum;
	static int yldkogumiMaht;
	static int valimiMaht;


	public static void main(String[] args) {

		/*
		 * Raami loomine ja kujundamine
		 */
		JFrame raam = new JFrame("Otsustaja");
		raam.setSize(510, 200);
		raam.setLocation(100, 100);
		raam.setLayout(new BorderLayout());
		GridBagConstraints c = new GridBagConstraints();
		raam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Tausta lisamine
		Calendar kalender = Calendar.getInstance();
		//kalender.setTimeZone(TimeZone.getTimeZone("EET"));
		int p2ev = kalender.get(Calendar.DAY_OF_WEEK);
		String pildifail = null;

		switch (p2ev) {
		case 1:  pildifail = "e.jpeg"; break;
		case 2:  pildifail = "t.jpg"; break;
		case 3:  pildifail = "k.jpeg"; break;
		case 4:  pildifail = "n.jpg"; break;
		case 5:  pildifail = "r.jpg"; break;
		case 6:  pildifail = "l.jpg"; break;
		case 7:  pildifail = "p.jpg"; break;
		}
		//System.out.println(pildifail); //kontroll

		/*String kataloogitee = Otsustaja.class.getResource(".").getPath(); // punkt tähistab jooksvat kataloogi
		//System.out.println(kataloogitee); //kontroll
		kataloogitee.replaceAll("build/classes", pildifail);
		Image pilt = (new ImageIcon(kataloogitee)).getImage();*/
		Image pilt = (new ImageIcon("/home/ksarv/Documents/git/Otsustaja/" + pildifail)).getImage();
		Image uusPilt = pilt.getScaledInstance(800, 538, Image.SCALE_DEFAULT);
		JLabel taust = new JLabel(new ImageIcon(uusPilt));
		raam.add(taust);
		taust.setLayout(new GridBagLayout());

		// Programmi kirjelduse lisamine
		JLabel kirjeldus = new JLabel("<html> &nbsp; Antud programm on abiks"
				+ " etteantud võimalustest <br>"
				+ " &nbsp; valiku tegemisel. <br><br><br></html>" + "\n");
		kirjeldus.setFont(new Font("Helvetica", Font.BOLD, 14));
		kirjeldus.setForeground(Color.white);
		c.weightx = 1; // tulba kaal (kuidas jaotatakse üleliigne ruum)
		c.fill = GridBagConstraints.HORIZONTAL; 
		c.gridwidth = 2; // lahtrite arv reas
		c.gridx = 0;
		c.gridy = 0;
		taust.add(kirjeldus, c);

		JLabel tekstiv2li1Teade = new JLabel(" Sisestage andmete asukoht:");
		// /Users/kai/Documents/ITK/Programmeerimise algkursus/git/Otsustaja/sokid.txt
		tekstiv2li1Teade.setFont(new Font("Helvetica", Font.BOLD, 13));
		tekstiv2li1Teade.setForeground(Color.white);
		c.weightx = 0.2; 
		c.fill = GridBagConstraints.HORIZONTAL; 
		c.gridwidth = 2; 
		c.gridx = 0;
		c.gridy = 1;
		taust.add(tekstiv2li1Teade, c);


		final JTextField tekstiv2li1 = new JTextField(30);
		c.weightx = 0.8;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		taust.add(tekstiv2li1, c);

		JLabel tekstiv2li2Teade = new JLabel(" Sisestage valiku suurus ja vajutage \"Otsusta!\": ");
		tekstiv2li2Teade.setFont(new Font("Helvetica", Font.BOLD, 13));
		tekstiv2li2Teade.setForeground(Color.white);
		c.weightx = 0.2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 2;
		taust.add(tekstiv2li2Teade, c);

		final JTextField tekstiv2li2 = new JTextField(6);
		c.weightx = 0.8;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 2;
		c.gridx = 1;
		c.gridy = 2;
		taust.add(tekstiv2li2, c);

		tekstiv2li2.setToolTipText(" Valiku suurus peab olema nullist suurem täisarv, "
				+ "mis on väiksem valikuvõimaluste arvust!");

		JButton submit = new JButton("Otsusta!");
		submit.setFont(new Font("Helvetica", Font.PLAIN, 12));
		c.weightx = 0;
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 3;
		taust.add(submit, c);

		raam.setVisible(true);


		/**
		 * Valimi leidmine pärast nupuvajutust
		 */
		ActionListener nupuListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				yldkogumiMaht = 0;
				valimiMaht = 0;

				yldkogumistValim(tekstiv2li1, tekstiv2li2);

			} // actionPerformed
		}; // nupuListener

		submit.addActionListener(nupuListener);

	}

	/**
	 * Meetod, mis loeb sisse üldkogumi ning leiab üldkogumimahu
	 * Seejärel rakendab meetodit valim().
	 * @param tekstiv2li1 - Väli, kus on kirjas ülkogumi asukoht
	 * @param tekstiv2li2 - Väli, kus on kirjas valimi asukoht
	 */
	static void yldkogumistValim(JTextField tekstiv2li1, JTextField tekstiv2li2) {
		try {
			String fail = tekstiv2li1.getText();
			String sisse;
			BufferedReader input = new BufferedReader(new FileReader(fail));
			while ((sisse = input.readLine()) != null) {
				yldkogum = sisse.split(", ");
			}
			yldkogumiMaht = yldkogum.length;
			System.out.println("Üldkogumimaht: " + yldkogumiMaht);
			input.close();

			valim(tekstiv2li2);

		} catch (NullPointerException npe) {
			System.out.println("Viga: " + npe);
			JTextArea veateade = new JTextArea("Viidatavat faili ei eksisteeri!" + "\n"
					+ "Sisestage registrifaili asukoht uuesti.");
			(veateade).setEditable(false);
			veateateRaam(veateade, 120);
		} catch (FileNotFoundException fnfe) {
			System.out.println("Viga: " + fnfe);
			JTextArea veateade = new JTextArea("Süsteem ei leia määratud faili!" + "\n"
					+ "Sisestage registrifaili asukoht uuesti.");
			(veateade).setEditable(false);
			veateateRaam(veateade, 120);
		} catch (IOException ioe) {
			System.out.println("Viga: " + ioe);
			JTextArea veateade = new JTextArea("Viga faili lugemisel!" + "\n"
					+ "Proovige uuesti.");
			(veateade).setEditable(false);
			veateateRaam(veateade, 120);
		} //catch

	} // yldkogum

	/** 
	 * Meetod, mis leiab valimi ja esitab tulemused
	 * konsoolis, vastavas graafilises väljas
	 * ning loeb need ka faili.
	 * @param tekstiv2li2 - Väli, kus on kirjas valimi asukoht
	 */
	static void valim(JTextField tekstiv2li2) {
		try {
			valimiMaht = Integer.parseInt(tekstiv2li2.getText());

			if (yldkogumiMaht < valimiMaht)
				throw new ValimimahuException(
						"Valiku suurus peab olema väiksem valikuvõimaluste arvust " + yldkogumiMaht + "!");
			System.out.println("Valimimaht: " + valimiMaht);

			try {
				if (valimiMaht == 0) {
					throw new NegativeArraySizeException();
				}

				int[] valimiIndeksid = new int[valimiMaht];
				String valim;

				JuhuslikValim a = new JuhuslikValim();
				valimiIndeksid = a.juhuslikValim(yldkogumiMaht, valimiMaht);
				valim = a.toString(yldkogum, valimiIndeksid);

				// Raami loomine ja kujundamine valimi kuvamiseks
				JFrame raam2 = new JFrame("Valim"); 
				raam2.setSize(350, 200);
				raam2.setLocation(105, 400);
				raam2.setLayout(new FlowLayout());
				Image pilt2 = (new ImageIcon("/home/ksarv/Documents/git/Otsustaja/valimipilt.jpg")).getImage();
				Image uusPilt2 = pilt2.getScaledInstance(500, 336, Image.SCALE_DEFAULT);
				JLabel taust2 = new JLabel(new ImageIcon(uusPilt2));
				raam2.add(taust2);
				taust2.setLayout(new FlowLayout());

				// Valimi viisakas väljastamine
				System.out.println ("Valim: " + valim);
				//http://stackoverflow.com/questions/1842223/java-linebreaks-in-jlabels
				JLabel valimPrint = new JLabel("<html><br>VALIMISSE KUULUVAD:<br><br> " + valim + "</html>");
				valimPrint.setFont(new Font("Helvetica", Font.BOLD, 14));
				valimPrint.setForeground(Color.white);
				taust2.add(valimPrint);

				raam2.setVisible(true);

				// Valimi faili kirjutamine	    	
				valimFaili(valim, raam2);

			} catch (NegativeArraySizeException nase) {
				JTextArea veateade = new JTextArea("Valiku suurus peab olema nullist suurem täisarv!" + 
						"\n" + "Sisestage valiku suurus uuesti.");
				(veateade).setEditable(false);
				veateateRaam(veateade, 110);
			} // catch

		} catch (NumberFormatException nfe) { // ei sisestatud üldse arvu
			System.out.println("\" " + tekstiv2li2.getText() +  "\" ei ole nullist suurem täisarv.");    			
			JTextArea veateade = new JTextArea("Valiku suurus peab olema nullist suurem täisarv!" + 
					"\n" + "Sisestage valiku suurus uuesti.");
			(veateade).setEditable(false);
			veateateRaam(veateade, 110);

		} catch (ValimimahuException ve) {
			System.out.println(ve.getMessage());  			
			JTextArea veateade = new JTextArea(ve.getMessage() + "\n" + "Sisestage valiku suurus uuesti.");
			(veateade).setEditable(false); // et ei saaks kirjeldust graafilises väljas muuta
			veateateRaam(veateade, 100);		    	
		}
	} // valimiMaht

	/**
	 * Meetod, mis salvestab loodud valimi tekstikujul faili
	 * @param valim - Valim Stringina
	 * @param raam - Raam, kus kuvada veateadet
	 */
	static void valimFaili (String valim, JFrame raam) {

		try {
			File fail = new File("valim.txt");
			// http://avajava.com/tutorials/lessons/how-do-i-write-an-object-to-a-file-and-read-it-back.html
			ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fail));
			output.writeObject(valim);
			output.close();

			ObjectInputStream input = new ObjectInputStream(new FileInputStream("valim.txt"));
			Object valimLoetud = input.readObject();
			input.close();
			System.out.println("\n" + valimLoetud);

		} catch(FileNotFoundException fnfe){ // kui faili ei saa sellesse kohta teha
			System.out.println("Viga: " +fnfe);
			JTextArea valimFailViga = new JTextArea("Valimi faili kirjutamine ebaõnnestus.");
			(valimFailViga).setEditable(false);
			raam.add(valimFailViga);
		} catch(IOException ioe){
			System.out.println("Viga: " + ioe);
		} catch(ClassNotFoundException cnfe){
			System.out.println("Viga: " + cnfe);
		} // catch
	}

	static void veateateRaam (JTextArea teade, int asukoht) {
		JFrame raam = new JFrame("Viga!"); // veateate jaoks raami loomine
		raam.setSize(400, 100);
		raam.setLocation(asukoht, 400);
		raam.setLayout(new FlowLayout());
		teade.setFont(new Font("Helvetica", Font.PLAIN, 12));
		raam.add(teade);
		raam.setVisible(true);
	}

}

