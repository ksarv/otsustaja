/**
 * Klass juhuslike valimite loomiseks tagasipanekuta.
 * 
 * @author Kai Sarv
 *
 */
public class JuhuslikValim {
 
	int juhuslikIndeks;
	
	/**
	 * Meetod, mis kontrollib, kas indeks on juba valimis.
	 * 
	 * @param valim
	 * @param valimiMaht
	 * @param juhuslikIndeks
	 * @return kasValimis
	 */
	boolean kasValimis(int[] valim, int valimiMaht, int juhuslikIndeks) {
		
		boolean kasValimis = false;
		
		for (int k = 0; k < valimiMaht; k++){
			if (valim[k] == juhuslikIndeks){
				kasValimis = true;
			}
		}
		return kasValimis;
	}
	
	/**
	 * Meetod, mis leiab etteantud üldkogumimahu ja valimimahu
	 * põhjal juhusliku valimi indeksid üldkogumis.
	 * Rakendab meetodit kasValimis(int[] valim, int valimiMaht, int juhuslikIndeks).
	 * 
	 * @param yldkogumiMaht
	 * @param valimiMaht
	 * @return
	 */
	int[] juhuslikValim(int yldkogumiMaht, int valimiMaht) {
		
		int[] valim = new int[valimiMaht];
		
		for (int i = 0; i < valimiMaht; i++){
	 		do {
	 			juhuslikIndeks = (int)(Math.random()*(yldkogumiMaht));
	 		}
	 		while (kasValimis(valim, i, juhuslikIndeks) == true);
	 		
	 		valim[i] = juhuslikIndeks;
		} // for
		
		String valimNr = "Valimi indeksid: " + "\n";

		for (int i = 0; i < valim.length; i++) {
			if ((i < valim.length - 1) && (i + 1) % 10 == 0)
				valimNr += valim[i] + ", " + "\n";
			else if (i < valim.length - 1)
				valimNr += valim[i] + ", ";
			else
				valimNr += valim[i] + ". ";
		} // for

		System.out.println(valimNr.replace("\n", ""));
		
		return valim;
	} // juhuslikValim

    /**
     * Meetod, mis leiab etteantud indeksite põhjal
     * üldkogumist valimi ning esitab selle trükikõlbliku Stringina.
     * 
     * @param yldkogum
     * @param valimiIndeksid
     * @return
     */
	public String toString(String[] yldkogum, int[] valimiIndeksid) {
		String valim = "";

		for (int i = 0; i < valimiIndeksid.length; i++) {
			if ((i != valimiIndeksid.length - 1) && (i + 1) % 3 == 0)
				valim += yldkogum[valimiIndeksid[i]] + ", " + "\n";
			else if (i != valimiIndeksid.length-1)
				valim = valim + yldkogum[valimiIndeksid[i]] + ", ";
			else
				valim = valim + yldkogum[valimiIndeksid[i]];
		}
		return valim;
	}
}

